## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Installeer Packages.R ####
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## R code voor Student Analytics Vrije Universiteit Amsterdam
## Copyright 2020 VU
## Web Page: http://www.vu.nl
## Contact: Theo Bakker (t.c.bakker@vu.nl)
## Verspreiding buiten de VU: Ja
##
## Doel: Definieer en installeer alle benodigde packages en voeg toe aan library
##
## Afhankelijkheden: Geen
##
## Datasets: Geen
##
## Opmerkingen:
## 1) Geen.
## 2) ___
##
## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## TODO:
## 1) ___.
##
## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Geschiedenis:
## 15-06-2020: JJ: Aanmaak bestand
## 15-06-2020: JJ: Toevoegen prints
## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Controleer of de library paths goed staan, voordat je deze code uitvoert.
if (!Sys.getenv('R_LIBS_USER') %in% .libPaths()) {
  if (.Platform$OS.type == "windows") {
    dir.create(Sys.getenv('R_LIBS_USER'), recursive = TRUE)
    stop(paste("De folder",
               Sys.getenv('R_LIBS_USER'), "Bestond niet en is nu aangemaakt: HERSTART R"))
  }
}

if (length(.libPaths()) == 1) {
  if (.Platform$OS.type == "windows") {
    stop("Er is maar 1 library path. controleer de instellingen")
  }
}

## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Definieer vector met benodigde packages
Packages <- c(
  "caret",          ## Gebruikt voor one hot encoding
  "cluster",        ## Gebruikt voor clustering (met gower distance)
  "dplyr",          ## Gebruikt voor de dplyr omgeving.
  "fst",            ## Gebruikt om bewerkingen te doen met grote data bestanden.
  "ggplot2",        ## Gebruikt om plots te maken
  "kableExtra",     ## Gebruikt voor vormgeving tabellen
  "lubridate",      ## Gebruikt om te werken met data en tijden.
  "mice",           ## Gebruikt voor imputeren van missende waarden
  "purrr",          ## Gebruikt om met functies and vectoren te werken.
  "readr",          ## Gebruikt om data (csv, tsv, and fwf) in te lezen.
  "reshape2",       ## Gebruikt voor reshaping van matrices
  "stats",          ## Gebruikt voor statisctische functies en berekeningen.
  "stringr",        ## Gebruikt voor mutaties op character strings
  "synthpop",       ## Gebruikt om dataset te simuleren
  "tibble",         ## Gebruikt voor bewerken en aanmaken van tibbles.
  "tidyr",          ## Gebruikt om data op te schonen in de tidverse omgeving.
  "utils",          ## Gebruikt voor utility functies
  "vroom"           ## Gebruikt om data (csv) sneller in te lezen.
)


# Installeer alleen de packages die nog niet geinstalleerd zijn.
Nieuwe_packages <-
  Packages[!(Packages %in% installed.packages()[, "Package"])]
if (length(Nieuwe_packages) > 0) {
  cat("Installeren packages...")
  install.packages(Nieuwe_packages,
                   dependencies = TRUE,
                   ## Geef de repository op, zodat installatie van packages ook
                   ## buiten rstudio werkt
                   repos = "https://cloud.r-project.org/")
  cat("\nGereed")
  
}


## Laad de packages in de library
cat("Inladen packages...")

lapply(Packages, library, character.only = TRUE)

cat("\nGereed")
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## RUIM OP ####
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

rm(Packages,
   Nieuwe_packages)

