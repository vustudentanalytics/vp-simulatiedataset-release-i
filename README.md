# README #

Deze README bevat de voorwaarden en de omschrijving voor het gebruik van de repository **vp-simulatiedataset**.

### Voorwaarden voor gebruik ##

Deze repository is tot stand gekomen dankzij de Zone Studiedata van het Versnellingsplan Onderwijsinnovatie met ICT. De voorwaarden voor gebruik zijn benoemd in de licentie voor gelijk delen m.b.t. niet commercieel gebruik.


### Doel van deze repository ###

Deze repository is bedoeld voor het creëren van een simulatiedataset. Het doel van de simulatiedataset is het beschikbaar stellen van data voor onderzoekers, beleidsmedewerkers en studenten, volgens de richtlijnen van de AVG. 

De simulatiedata heeft de volgende kwaliteitseisen:

* Simulatiedata moet tot dezelfde statistische inferenties leiden als de originele data.

* Simulatiedata moet inschrijf- en studievoortgangsdata van 25.000 studenten bevatten voor een periode van 10 jaar.

* Simulatiedata bevat data van een fictieve universiteit: Universiteit van Schipluiden en is gebaseerd op data van de EUR en de VU. 


### Installeren voor gebruik ###

Benodigdheden voor gebruik van deze repository zijn R en Rstudio. 
De gesimuleerde data en een samenvatting van het simulatieresultaat zijn te vinden in map 06. Simulatieresultaat.

In deze repository wordt veelvuldig gebruik gemaakt van functies uit het synthpop package (https://synthpop.org.uk/). Dit package dient geïnstalleerd te worden.

Daarnaast zijn de volgende andere packages nodig voor het gebruik:

* caret
* cluster
* dplyr
* fst
* ggplot2
* kableExtra
* lubridate
* mice
* purrr
* readr
* reshape2
* stats
* stringr
* synthpop
* tibble
* tidyr
* utils
* vroom


### Gebruik van het package ###

Het hoofdscript is het *Index.R* script. Dit script bestaat uit de volgende stappen:

* 00. VOORBEREIDEN: Bepalen van de directories, inlezen van de packages, functies en libraries en inlezen van regels waaraan de simulatie moet voldoen. 

* 01. SELECTEREN: Selecteren van de variabelen die nodig zijn voor de simulatie. Hierbij wordt een documentatiebestand aangemaakt in de Documentatie map met de variabelen en informatie. Deze wordt gebruikt bij het selecteren van de kolommen in de zelf opgegeven te simuleren data uit de Input map.

* 02. SIMULEREN: Vooraf bepalen van aantal simulatiedatasets, default is 1. Vertalen van opleidingsnamen voor de dataset (zelf op te geven in Documentatie map) en het simuleren van de data met behulp van het synthpop package.

+ 03. EVALUEREN: De evaluatie bestaat uit 2 stappen: evalueren correlaties en evalueren simulatie. 
De kwaliteit en bruikbaarheid van de gesimuleerde data wordt op twee specifieke onderdelen geevalueerd: 
  * De onderlinge correlaties tussen variabelen in de originele data blijven behouden in de simulatiedata.
  * De afwijking van de simulatiedata ten opzichte van de originele data is minimaal.

* 04. CONTROLEREN: Controle op logica en privacy. Logische verbanden worden gecheckt en rijen waarin de privacy niet gewaarborgd wordt worden verwijderd. 

* 05. RAPPORTEREN: De resultaten worden gerapporteerd in een markdown bestand. Hiervoor worden de evaluatiescripts gebruikt.

* Simulatieresultaat: Deze map bevat de uiteindelijk gecreeërde documenten

Daarnaast zijn er de volgende mappen voor tussentijdse opslag van de data:

* XX. DOCUMENTATIE: Bevat documentatie van de brondata en een documentatiebestand waarin opleidingen naar globale opleidingsnamen worden vertaald.

* XX. FUNCTIES: Bevat zelf geschreven functies die nodig zijn in het simulatieproces.

* XX. INPUT: Bevat de te simuleren brondata.

- XX. OUTPUT: Bevat outputbestanden per stap, waarbinnen onderscheid wordt gemaakt tussen:

  1. Geselecteerde data: Bevat de brondata met de te simuleren variabelen.
  
  2. Gesimuleerde data: Bevat de brondata die geïmputeerd is op missende waarden en de gesimuleerde data.
  
  3. Evaluatiedata: Bevat de gevonden verschillen in correlaties van variabelen-combinaties.


### Details omtrent keuzes ###

In het simulatieproces worden verschillende stappen uitgevoerd, waarin keuzes gemaakt moeten worden. Deze hangen af van de waarden die aan bepaalde variabelen worden meegegeven. 


In Index.R script worden de volgende variabelen aangemaakt:

* index: Geeft aan of index script wordt aangeroepen, waarbij alle stappen vanaf hier worden aangeroepen. Deze variabele is nodig in het maken van de markdown in de 05. RAPPORTEREN stap. Default is TRUE.

* Variabelen_selectie: Geeft de variabelen die dienen te worden geselecteerd in de brondata. Default is NULL. Bij NULL popt een keuzemenu op waarin handmatig de selectie kan worden gemaakt. Indien dit niet gewenst is, dient een lijst met de variabelen te worden meegegeven.

* Aantal_simulatiedatasets: Aantal simulaties dat wordt gemaakt. Default is 1. Bij meerdere simulaties zou verderop een keuze kunnen worden gemaakt welke simulatiedataset moet worden meegenomen. Dit is echter nog niet geïmplementeerd.

* seed: seed om de simulatie mee te beginnen. Staat op 1. 

In de 00. VOORBEREIDEN stap worden keuzes gemaakt over de regels, packages en functies die nodig zijn voor het simuleren.

In de 02. SIMULEREN stap worden de volgende variabelen aangemaakt:

* logfile: Boolean om eventueel een logfile aan te maken voor de simulatie.

* Aantal_partities: De data wordt in partities gesplit, om geheugen te sparen tijdens het imputeren van de missende waarden. Deze staat nu op 3.

* Clustering: Boolean die aangeeft of de studentnummers obv clustering moeten worden bepaald. Deze staat bij default op FALSE.


In de 03. EVALUEREN stap worden de volgende variabelen aangemaakt:

* Aantal_plots: deze variabele geeft het aantal plots aan dat meegenomen kan worden naar de markdown. Dit kunnen er niet al te veel zijn vanwege geheugen. Default is 10.


### Doorlooptijden voor de simulatie ###

De simulatie stap heeft redelijk wat tijd nodig. Bij een test met ongeveer 226.000 observaties van 106 variabelen deed de simulatie er naar schatting 3 uur over voor deze klaar was. De markdown met de evaluatie deed er naar schatting 30 minuten over. Dit kan afwijken bij andere data, maar dit geeft een ongeveer beeld van de tijd.

### Met dank aan ###

* Dominique van Deursen, Erasmus Universiteit Rotterdam, projectleider
* Katja van der Perk, Vrije Universiteit Amsterdam, student-assistent VU Analytics
* Jurriaan Janssen, Vrije Universiteit Amsterdam, student-assistent VU Analytics

### Contactgegevens ###

* Dominique van Deursen, Erasmus Universiteit Rotterdam, <d.l.vandeursen@eur.nl>
* Theo Bakker, Vrije Universiteit Amsterdam, <t.c.bakker@vu.nl>

